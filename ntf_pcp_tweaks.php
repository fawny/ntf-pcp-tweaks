<?php

require_once 'ntf_pcp_tweaks.civix.php';
use CRM_NtfPcpTweaks_ExtensionUtil as E;

/**
 * Implements hook_civicrm_buildForm() to add CSS to certain PCP forms.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 *
 */
function ntf_pcp_tweaks_civicrm_buildForm($formName, &$form) {
  if (!in_array($form->getVar('_id'), [8, 12, 20, 25, 31])) {
    return;
    //'Year-End Campaign 2019' => 8
    //'Year-End Campaign 2020' => 12
    //'2021_NATL_Year End Team Campaign umbrella' => 20
    //'2022_NATL_Year End Team Campaign umbrella' => 25
    //'2023_NATL_Year End Team Campaign umbrella' => 31
  }
  switch ($formName) {
    case 'CRM_Contribute_Form_Contribution_Main':
      // Hide "This contribution is being made thanks to the efforts of X, who supports our campaign."
      CRM_Core_Resources::singleton()->addStyle('.crm-section.pcpSupporterText-section { display: none; }');
      break;
    
    case 'CRM_Contribute_Form_Contribution_Confirm':
    case 'CRM_Contribute_Form_Contribution_ThankYou':
      // Hide "Contribution Honor Roll" section
      CRM_Core_Resources::singleton()->addStyle('.crm-group.pcp_display-group { display: none; }');
      break;
  }
}

/**
 * Implements hook_civicrm_pageRun() to add CSS to certain PCP pages.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_pageRun
 *
 */
function ntf_pcp_tweaks_civicrm_pageRun(&$page) {
  if ($page->getVar('_name') == 'CRM_PCP_Page_PCPInfo') {
    // remove border around thermometer and make its box opaque
    CRM_Core_Resources::singleton()->addStyle('#crm-container .pcp-widgets { border: none; background-color: #fff; }');
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function ntf_pcp_tweaks_civicrm_config(&$config) {
  _ntf_pcp_tweaks_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function ntf_pcp_tweaks_civicrm_install() {
  _ntf_pcp_tweaks_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function ntf_pcp_tweaks_civicrm_enable() {
  _ntf_pcp_tweaks_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function ntf_pcp_tweaks_civicrm_navigationMenu(&$menu) {
  _ntf_pcp_tweaks_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _ntf_pcp_tweaks_civix_navigationMenu($menu);
} // */

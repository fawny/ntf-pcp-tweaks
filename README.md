# ntf-pcp-tweaks

Response to these requests:

- On PCPs, remove language “This contribution is being made thanks to the effort of -----, who supports our campaign”
- remove from PCP confirmation page: “Don't list my contribution in the honor roll.“ 
- remove box around the thermometer

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Written for

* PHP v7.0+
* CiviCRM 5.19